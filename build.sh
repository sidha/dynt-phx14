#!/bin/sh

# Remove old releases
rm -rf _build/prod/rel/*

# Build the image
docker build --rm -t dynt-build -f Dockerfile.build .

# Run the container
docker run -it --rm --name dynt-build -v $(pwd)/_build/prod/rel:/opt/app/_build/prod/rel dynt-build
